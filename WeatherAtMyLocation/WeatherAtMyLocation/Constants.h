//
//  Constants.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#ifndef WeatherAtMyLocation_Constants_h
#define WeatherAtMyLocation_Constants_h

//notification param names
#define LOCATION @"LOCATION"

#endif
