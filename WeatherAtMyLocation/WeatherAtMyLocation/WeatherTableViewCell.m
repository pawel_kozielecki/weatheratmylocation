//
//  WeatherTableViewCell.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 07/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "WeatherTableViewCell.h"

@implementation WeatherTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setText:(NSString*)text {
    self.label.text = text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
