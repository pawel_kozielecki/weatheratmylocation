//
//  Util.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "Util.h"


@implementation Util

+ (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message {
    runOnMainQueueWithoutDeadlocking(^(void){
        [UIAlertView showWithTitle:title
                           message:message
                 cancelButtonTitle:@"Ok"
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              
                          }];
    });
}

+ (void)showProgressIndicatorWithMessage:(NSString *)message forView:(UIView*)view {
    if (!view)
        return;
    runOnMainQueueWithoutDeadlocking(^(void){
        MBProgressHUD *hud = [MBProgressHUD HUDForView:view];
        if (hud)
            [hud hide:NO];
        hud = [[MBProgressHUD alloc] initWithView:view];
        hud.color = [UIColor whiteColor];
        hud.activityIndicatorColor = [UIColor colorWithRed:0.0f green:174.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
        hud.labelColor = [UIColor colorWithRed:0.0f green:174.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
        [view addSubview:hud];
        hud.labelText = message;
        [hud show:YES];
    });
}

+ (void)showProgressIndicatorWithMessage:(NSString*)message {
    UIView *view = [self getCurrentView];
    [Util showProgressIndicatorWithMessage:message forView:view];
}

+ (void)hideProgressIndicatorForView:(UIView*)view {
    if (!view)
        return;
    //
    runOnMainQueueWithoutDeadlocking(^(void){
        [MBProgressHUD hideAllHUDsForView:view animated:YES];
    });
}

+ (UIView *)getCurrentView {
    UIView *view = [AppDelegate appDelegate].window.rootViewController.view;
    return view;
}

+ (void)hideProgressIndicator {
    UIView *view;
    view = [self getCurrentView];
    [Util hideProgressIndicatorForView:view];
}

+ (UIView*)viewFromNib:(NSString*)viewName {
    return [[[NSBundle mainBundle] loadNibNamed:viewName owner:nil options:nil] objectAtIndex:0];
}


void runOnMainQueueWithoutDeadlocking(void (^block)(void)) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

@end
