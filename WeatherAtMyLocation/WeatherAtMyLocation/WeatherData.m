//
//  WeatherData.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 07/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "WeatherData.h"
#import "KZPropertyMapper.h"
#import "KZPropertyMapperCommon.h"
#import "KZPropertyDescriptor.h"
#import "HHUnitConverter.h"

@interface WeatherData()

@property (strong, nonatomic) NSArray *formattedDataSource;

@end

@implementation WeatherData

- (id)initFromServerData:(NSDictionary*)data {
    self = [super init];
    if (self) {
        [self mapServerData:data];
    }
    return self;
}

#pragma mark -
#pragma mark mapping server response to properties
- (void)mapServerData:(NSDictionary*)data {
    [KZPropertyMapper mapValuesFrom:data
                         toInstance:self
                       usingMapping:@{
                                      @"main" : @{
                                              @"humidity"   : KZProperty(humidity),
                                              @"pressure"   : KZProperty(pressure),
                                              @"temp"       : KZProperty(currentTemperature),
                                              @"temp_max"   : KZProperty(maxTemperature),
                                              @"temp_min"   : KZProperty(minTemperature)
                                              },
                                      @"name" : KZProperty(city),
                                      @"weather" : @{
                                                @0 :    @{
                                                        @"description" : KZProperty(weatherDescription)
                                                        }
                                              },
                                      @"wind" : @{
                                              @"speed" : KZProperty(windSpeed)
                                              }
                                      }];
}

- (HHUnitConverter*)createUnitConverter {
    HHUnitConverter *converter = [HHUnitConverter new];
    [converter letUnit:@"K" convertToUnit:@"C" byAdding:-273];
    return converter;
}

#pragma mark -
#pragma mark parsing data into NSArray data provider (suitable for UITableView)
- (NSArray*)tableViewDataSource {
    if (!self.formattedDataSource)
        self.formattedDataSource = [self createTableViewDataSource];
    return self.formattedDataSource;
}

- (NSArray*)createTableViewDataSource {
    HHUnitConverter *converter = [self createUnitConverter];
    NSNumber *currentTemp = [converter value:self.currentTemperature convertedFromUnit:@"K" toUnit:@"C"];
    NSNumber *maxTemp = [converter value:self.maxTemperature convertedFromUnit:@"K" toUnit:@"C"];
    NSNumber *minTemp = [converter value:self.minTemperature convertedFromUnit:@"K" toUnit:@"C"];
    return [NSArray arrayWithObjects:
            [NSString stringWithFormat:NSLocalizedString(@"Description: %@", nil),self.weatherDescription],
            [NSString stringWithFormat:NSLocalizedString(@"Current temperature: %@°C", nil),currentTemp],
            [NSString stringWithFormat:NSLocalizedString(@"Max temperature: %@°C", nil),minTemp],
            [NSString stringWithFormat:NSLocalizedString(@"Min temperature: %@°C", nil),maxTemp],
            [NSString stringWithFormat:NSLocalizedString(@"Pressure: %luHPa", nil),(unsigned long)self.pressure],
            [NSString stringWithFormat:NSLocalizedString(@"Humidity: %lu%%", nil),(unsigned long)self.humidity],
            [NSString stringWithFormat:NSLocalizedString(@"Wind speed: %lu m/s", nil),(unsigned long)self.windSpeed],
            nil];
}

#pragma mark -
#pragma mark data validation
- (BOOL)isValid {
    return self.currentTemperature > 0;
}

@end
