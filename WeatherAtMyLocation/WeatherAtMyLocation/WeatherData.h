//
//  WeatherData.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 07/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherData : NSObject

@property (nonatomic) NSUInteger maxTemperature;
@property (nonatomic) NSUInteger minTemperature;
@property (nonatomic) NSUInteger currentTemperature;
@property (nonatomic) NSUInteger pressure;
@property (nonatomic) NSUInteger humidity;
@property (nonatomic) NSUInteger windSpeed;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *weatherDescription;

- (id)initFromServerData:(NSDictionary*)data;
- (NSArray*)tableViewDataSource;
- (BOOL)isValid;

@end
