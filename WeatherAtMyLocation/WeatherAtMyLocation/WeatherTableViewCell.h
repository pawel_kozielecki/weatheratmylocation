//
//  WeatherTableViewCell.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 07/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;

- (void)setText:(NSString*)text;

@end
