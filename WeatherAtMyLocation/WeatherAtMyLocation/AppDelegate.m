//
//  AppDelegate.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "AppDelegate.h"
#import "WeatherViewController.h"
#import "LocationService.h"
#import "WeatherInfoService.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self createModel];
    [self createServices];
    [self createInitialView];
    
    return YES;
}

- (void)createModel {
    
}

- (void)createServices {
    self.locationService = [LocationService sharedInstance];
    self.weatherService = [WeatherInfoService sharedInstance];
}

- (void)createInitialView {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    WeatherViewController *initialView = [[WeatherViewController alloc] initWithLocationService:self.locationService
                                                                              andWeatherService:self.weatherService];
    self.window.rootViewController = initialView;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
}



+ (AppDelegate*) appDelegate {
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}
                                      

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
