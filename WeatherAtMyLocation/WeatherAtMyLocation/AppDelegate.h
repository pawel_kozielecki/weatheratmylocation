//
//  AppDelegate.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationService.h"
#import "WeatherInfoService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) LocationService *locationService;
@property (strong, nonatomic) WeatherInfoService *weatherService;


+ (AppDelegate*) appDelegate;

@end

