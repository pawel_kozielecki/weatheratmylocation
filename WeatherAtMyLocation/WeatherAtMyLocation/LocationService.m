//
//  LocationService.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "LocationService.h"
#import "Util.h"
#import "Constants.h"

#define MAX_LOCATION_UPDATES_COUNT 30

@interface LocationService()

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic) NSUInteger locationUpdateCount;

@end


@implementation LocationService

+ (LocationService*)sharedInstance {
    static LocationService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        self.locationUpdateCount = 0;
        [self initLocationManager];
    }
    return self;
}
- (void)initLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    [self startGatheringUserPosition];
}

- (void)startUpdatingLocation {
    self.locationUpdateCount = 0;
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
}

- (void)startGatheringUserPosition {
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    } else {
        [self startUpdatingLocation];
    }
}

#pragma mark -
#pragma mark location permissions for iOS8
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
            break;
            
        case kCLAuthorizationStatusDenied:
            [self dispatchLocationUpdateFailedNotification];
            break;
            
        default:
            [self startUpdatingLocation];
            break;
    }
}

#pragma mark -
#pragma mark location update handler
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = newLocation;
    self.locationUpdateCount++;
    [self checkUpdateCount];
    [self dispatchCurrentLocation];
    DebugLog(@"-- location update, lon: %f lat: %f", newLocation.coordinate.longitude, newLocation.coordinate.latitude);
}

- (void)checkUpdateCount {
    if (self.locationUpdateCount >= MAX_LOCATION_UPDATES_COUNT) {
        [self stopUpdatingLocation];
    }
}

- (void)dispatchCurrentLocation {
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[self.currentLocation]
                                                     forKeys:@[LOCATION]];
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOCATION_UPDATED object:self userInfo:dict];
}
- (void)dispatchLocationUpdateFailedNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOCATION_UNAVALIABLE object:self userInfo:nil];
}

@end
