//
//  WeatherInfoService.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 07/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "WeatherInfoService.h"
#import "AFNetworking.h"
#import "Util.h"
#import "Constants.h"
#import "KZPropertyMapper.h"


#define WEATHER_API_URL_BASE @"http://api.openweathermap.org/data/2.5/weather?"
#define REQUEST_TIMEOUT 60;

@interface WeatherInfoService()

@property (strong, nonatomic) AFHTTPRequestOperationManager *connectionManager;

@end


@implementation WeatherInfoService

+ (WeatherInfoService*)sharedInstance {
    static WeatherInfoService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        [self initConnectionManager];
        self.isUpdating = NO;
    }
    return self;
}
- (void)initConnectionManager {
    self.connectionManager = [AFHTTPRequestOperationManager manager];
    self.connectionManager.requestSerializer.timeoutInterval = REQUEST_TIMEOUT;
}

- (void)getCurrentWeatherAtLocation:(CLLocation*)location withCompletionBlock:(void (^)(WeatherData *data, NSError *error))completionBlock{
    self.isUpdating = YES;
    NSString *url = [self createWeatherApiUrlForLocation:location];
    [self.connectionManager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.isUpdating = NO;
        WeatherData *data = [[WeatherData alloc] initFromServerData:responseObject];
        completionBlock(data, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.isUpdating = NO;
        DebugLog(@"----WeatherInfoService: Error getting weather info: %@",error);
        completionBlock(nil, error);
    }];
}

- (NSString*)createWeatherApiUrlForLocation:(CLLocation *)location {
    return [NSString stringWithFormat:@"%@lat=%f&lon=%f", WEATHER_API_URL_BASE, location.coordinate.latitude, location.coordinate.longitude];
}


@end
