//
//  LocationService.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

#define USER_LOCATION_UPDATED @"USER_LOCATION_UPDATED"
#define USER_LOCATION_UNAVALIABLE @"USER_LOCATION_UNAVALIABLE"

@interface LocationService : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocation *currentLocation;

+ (LocationService*)sharedInstance;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;

@end
