//
//  WeatherViewController.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationService.h"
#import "WeatherInfoService.h"

@interface WeatherViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *refreshLocationButton;
@property (strong, nonatomic) IBOutlet UIButton *refreshWeatherDataButton;

@property (strong, nonatomic) LocationService *locationService;
@property (strong, nonatomic) WeatherInfoService *weatherService;

- (id)initWithLocationService:(LocationService*)locationService andWeatherService:(WeatherInfoService*)weatherService;

@end
