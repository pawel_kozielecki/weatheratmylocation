//
//  WeatherInfoService.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 07/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "WeatherData.h"

@interface WeatherInfoService : NSObject

@property (nonatomic) BOOL isUpdating;

+ (WeatherInfoService*)sharedInstance;

- (void)getCurrentWeatherAtLocation:(CLLocation*)location withCompletionBlock:(void (^)(WeatherData *data, NSError *error))completionBlock;

@end
