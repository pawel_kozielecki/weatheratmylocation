//
//  Util.h
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIAlertView+Blocks.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#if DEBUG == 1
#define DebugLog(...) NSLog(__VA_ARGS__)
#else
#define DebugLog(...)
#endif

@interface Util : NSObject

+ (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message;

+ (void)showProgressIndicatorWithMessage:(NSString *)message forView:(UIView*)view;

+ (void)showProgressIndicatorWithMessage:(NSString*)message;

+ (void)hideProgressIndicatorForView:(UIView*)view;

+ (UIView *)getCurrentView;

+ (void)hideProgressIndicator;

+ (UIView*)viewFromNib:(NSString*)viewName;


void runOnMainQueueWithoutDeadlocking(void (^block)(void));


@end
