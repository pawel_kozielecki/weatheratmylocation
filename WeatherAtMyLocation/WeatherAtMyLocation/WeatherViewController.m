//
//  WeatherViewController.m
//  WeatherAtMyLocation
//
//  Created by Pawel Kozielecki on 06/02/15.
//  Copyright (c) 2015 Two Goats Software LTD. All rights reserved.
//

#import "WeatherViewController.h"
#import "WeatherData.h"
#import "Util.h"
#import "WeatherTableViewCell.h"

#define WEATHER_TABLE_CELL_ID @"WeatherTableViewCell"

@interface WeatherViewController ()

@property (strong, nonatomic) WeatherData *weatherData;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation WeatherViewController

- (id)initWithLocationService:(LocationService*)locationService andWeatherService:(WeatherInfoService*)weatherService {
    self = [super init];
    if (self) {
        self.locationService = locationService;
        self.weatherService = weatherService;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self initListeners];
    [self initRefreshControl];
    [self initViews];
}

- (void)initListeners {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onUserLocationUpdate:)
                                                 name:USER_LOCATION_UPDATED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onUserLocationNotAvailable:)
                                                 name:USER_LOCATION_UNAVALIABLE
                                               object:nil];
}

- (void)initRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshWeatherData) forControlEvents:UIControlEventValueChanged];
}

- (void)initViews {
    [self initTableView];
    self.refreshLocationButton.hidden = YES;
    self.descriptionLabel.hidden = YES;
    self.locationLabel.hidden = YES;
    self.refreshWeatherDataButton.hidden = YES;
    [self showProgressWhileObtainingLocation];
}

- (void)initTableView {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.hidden = YES;
}

- (void)showProgressWhileObtainingLocation {
    [Util showProgressIndicatorWithMessage:NSLocalizedString(@"Obtaining location...", nil) forView:self.view];
}
- (void)showProgressWhileObtainingWeatherInfo {
    [Util showProgressIndicatorWithMessage:NSLocalizedString(@"Downloading weather data...", nil) forView:self.view];
}
- (void)hideProgressIndicator {
    [Util hideProgressIndicatorForView:self.view];
}

- (void)updateViews {
    [self.tableView reloadData];
    self.tableView.hidden = NO;
    self.tableView.userInteractionEnabled = YES;
    self.descriptionLabel.text = NSLocalizedString(@"pull down to refresh the weather forecast!", nil);
    self.descriptionLabel.hidden = NO;
    self.refreshLocationButton.hidden = NO;
    self.refreshLocationButton.userInteractionEnabled = YES;
    [self displayLocationInfo];
}

- (void)updateViewsWithNoInternetConnection {
    self.descriptionLabel.text = NSLocalizedString(@"no internet connection!", nil);
    self.descriptionLabel.hidden = NO;
    self.refreshWeatherDataButton.hidden = NO;
    self.refreshWeatherDataButton.userInteractionEnabled = YES;
    [self displayLocationInfo];
}

- (void)displayLocationInfo {
    self.locationLabel.hidden = NO;
    self.locationLabel.text = [NSString stringWithFormat:NSLocalizedString(@"You are in: %@", nil), [self getLocationName]];
}
- (NSString*)getLocationName {
    if (self.weatherData == nil) {
        return @"undetermined location";
    } else {
        return self.weatherData.city;
    }
}

#pragma mark -
#pragma mark fourcefull refresh location update
- (IBAction)onRefreshLocation:(id)sender {
    [self.locationService startUpdatingLocation];
    self.refreshLocationButton.userInteractionEnabled = NO;
    self.tableView.userInteractionEnabled = NO;
    self.weatherData = nil;
}


#pragma mark -
#pragma mark forcefull reload of weather data (when internet connection failed)
- (IBAction)onReloadWeatherData:(id)sender {
    self.refreshWeatherDataButton.userInteractionEnabled = NO;
    self.tableView.userInteractionEnabled = NO;
    [self showProgressWhileObtainingWeatherInfo];
    [self refreshWeatherData];
}


#pragma mark -
#pragma mark weather table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.weatherData.tableViewDataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeatherTableViewCell *cell = (WeatherTableViewCell*)[tableView dequeueReusableCellWithIdentifier:WEATHER_TABLE_CELL_ID];
    if (cell == nil) {
        cell = (WeatherTableViewCell*)[Util viewFromNib:WEATHER_TABLE_CELL_ID];
    }
    NSUInteger rowindex = indexPath.row;
    [cell setText:self.weatherData.tableViewDataSource[rowindex]];
    return cell;
}
#pragma mark cell insets fix for iOS8
- (void)tableView:(UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -
#pragma mark refreshing weather data
- (void)refreshWeatherData {
    [self.weatherService getCurrentWeatherAtLocation:self.locationService.currentLocation
                               withCompletionBlock:^(WeatherData *data, NSError *error) {
                                   [self.locationService stopUpdatingLocation];
                                   [self hideProgressIndicator];
                                   if (error == nil) {
                                       self.weatherData = data;
                                       [self updateViews];
                                   } else {
                                       [self showAlertForWeatherServiceError:error];
                                       [self updateViewsWithNoInternetConnection];
                                   }
                                   [self.refreshControl endRefreshing];
                               }];
}

#pragma mark -
#pragma mark location unavaliable
- (void)onUserLocationNotAvailable:(NSNotification*)notification {
    [Util hideProgressIndicatorForView:self.view];
    [self showInfoToEnableLocations];
}

- (void)showInfoToEnableLocations {
    self.descriptionLabel.text = NSLocalizedString(@"Location service is disabled", nil);
    self.descriptionLabel.hidden = NO;
}


#pragma mark -
#pragma mark obtaining whather data
- (void)onUserLocationUpdate:(NSNotification*)notification {
    if ([self canSendWeatherQuery]) {
        [self hideProgressIndicator];
        [self showProgressWhileObtainingWeatherInfo];
        [self refreshWeatherData];
    }
}

- (void)showAlertForWeatherServiceError:(NSError *)error {
    [Util showAlertWithTitle:NSLocalizedString(@"Unable to obtain weather data", nil)
                  andMessage:[NSString stringWithFormat:NSLocalizedString(@"Error: %@", nil), error.localizedDescription]];
}

- (BOOL)canSendWeatherQuery {
    return  ![self hasValidWeatherData] &&
            !self.weatherService.isUpdating;
}

- (BOOL)hasValidWeatherData {
    return self.weatherData && [self.weatherData isValid];
}

@end
